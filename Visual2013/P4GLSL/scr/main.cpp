﻿#include "BOX.h"
#include "auxiliar.h"
#include "PLANE.h"

#include <windows.h>

#include <gl/glew.h>
#include <gl/gl.h>
#define SOLVE_FGLUT_WARNING
#include <gl/freeglut.h> 
#include <iostream>
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <list>
#include <cstdio>
#include <array>

#define RAND_SEED 31415926
#define SCREEN_SIZE 640,480
#define CURVE_POINTS 100

// Perlin noise constants
const int PerlinN_Width = 512;
const int PerlinN_Height = 512;

int  PerlinN_GridSize = 8;
const int  PerlinN_FbmLayers = 8;

// Cell noise constants
const int CellN_Width = 512;
const int CellN_Height = 512;

const int  CellN_GridSize = 32;

int currentPoint = 0;

//////////////////////////////////////////////////////////////
// Datos que se almacenan en la memoria de la CPU
//////////////////////////////////////////////////////////////

//Matrices
glm::mat4	proj = glm::mat4(1.0f);
glm::mat4	view = glm::mat4(1.0f);
glm::mat4	model = glm::mat4(1.0f);


//////////////////////////////////////////////////////////////
// Variables que nos dan acceso a Objetos OpenGL
//////////////////////////////////////////////////////////////
float angle = 0.0f;

//VAO
unsigned int vao;

//VBOs que forman parte del objeto
unsigned int posVBO;
unsigned int colorVBO;
unsigned int normalVBO;
unsigned int texCoordVBO;
unsigned int triangleIndexVBO;

unsigned int colorTexId;

//Por definir
unsigned int vshader;
unsigned int fshader;
unsigned int program;

//Variables Uniform 
int uModelViewMat;
int uModelViewProjMat;
int uNormalMat;

//Texturas Uniform
int uColorTex;

//Atributos
int inPos;
int inColor;
int inNormal;
int inTexCoord;

//////////////////////////////////////////////////////////////
// Funciones auxiliares
//////////////////////////////////////////////////////////////

//Declaración de CB
void renderFunc();
void resizeFunc(int width, int height);
void idleFunc();
void keyboardFunc(unsigned char key, int x, int y);
void mouseFunc(int button, int state, int x, int y);

void renderCube();
void renderPlane();

//Funciones de inicialización y destrucción
void initContext(int argc, char** argv);
void initOGL();
void initShaderFw(const char *vname, const char *fname);
void initObj();
void destroy();


//Carga el shader indicado, devuele el ID del shader
//!Por implementar
GLuint loadShader(const char *fileName, GLenum type);

//Crea una textura, la configura, la sube a OpenGL, 
//y devuelve el identificador de la textura 
//unsigned int PerlinN_LoadTex();

//////////////////////////////////////////////////////////////
// Nuevas variables auxiliares
//////////////////////////////////////////////////////////////

//static GLubyte texData[PerlinN_Height][PerlinN_Width][4];
static GLubyte texData[CellN_Height][CellN_Width][4];

static int PerlinN_TexAux1[PerlinN_Height][PerlinN_Width][4];
static int PerlinN_TexAux2[PerlinN_Height+1][PerlinN_Width+1][4];

static int CellN_TexAux[CellN_Height + 2*CellN_GridSize][CellN_Width + 2*CellN_GridSize][4];
std::list<int> CellN_FPX;
std::list<int> CellN_FPY;

glm::vec3 BezierPoints[CURVE_POINTS * 4];
glm::vec3 BezierTangents[CURVE_POINTS * 4];
int points = 0;


//////////////////////////////////////////////////////////////
// Nuevas funciones auxiliares
//////////////////////////////////////////////////////////////

// Perlin Noise Functions
unsigned int PerlinN_LoadTex();
void PerlinN_GenerateTex();
void PerlinN_InitTex();
void PerlinN_InterpolateTex();

// Cell Noise Functions
unsigned int CellN_LoadTex();
void CellN_GenerateTex();
void CellN_InitTex();

// Bezier Functions
void AddBezierCurve(glm::vec3 a, glm::vec3 b, glm::vec3 c, glm::vec3 d);
glm::vec3 CalculateBezierPoint(float t,
	glm::vec3 p0, glm::vec3 p1, glm::vec3 p2, glm::vec3 p3);
glm::vec3 CalculateBezierPointTangent(float t, glm::vec3 p0, glm::vec3 p1, glm::vec3 p2, glm::vec3 p3);

int main(int argc, char** argv)
{
	std::locale::global(std::locale("spanish"));// acentos ;)
	srand(RAND_SEED);

	initContext(argc, argv);
	initOGL();
	initShaderFw("../shaders_P4/fwRendering.v0.vert", "../shaders_P4/fwRendering.v0.frag");
	initObj();

	glutMainLoop();
	
	destroy();

	return 0;
}

//////////////////////////////////////////
// Funciones auxiliares 
void initContext(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitContextVersion(3, 3);
	glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);
	glutInitContextProfile(GLUT_CORE_PROFILE);
	//glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowSize(SCREEN_SIZE);
	glutInitWindowPosition(0, 0);
	glutCreateWindow("Canon Antilope");

	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		std::cout << "Error: " << glewGetErrorString(err) << std::endl;
		exit(-1);
	}

	const GLubyte *oglVersion = glGetString(GL_VERSION);
	std::cout << "This system supports OpenGL Version: " << oglVersion << std::endl;

	glutReshapeFunc(resizeFunc);
	glutDisplayFunc(renderFunc);
	glutIdleFunc(idleFunc);
	glutKeyboardFunc(keyboardFunc);
	glutMouseFunc(mouseFunc);
}

void initOGL()
{
	glEnable(GL_DEPTH_TEST);
	glClearColor(0.2f, 0.2f, 0.2f, 0.0f);

	glFrontFace(GL_CCW);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glEnable(GL_CULL_FACE);

	proj = glm::perspective(glm::radians(60.0f), 1.0f, 1.0f, 50.0f);
	view = glm::mat4(1.0f);

	// Bezier Curves
	/*glm::vec3 A(-10.0, 10.0, 0.0);
	glm::vec3 b(-15.0, 5.0, 0.0);
	glm::vec3 c(-15.0, -5.0, 0.0);
	glm::vec3 D(-10.0, -10.0, 0.0);
	glm::vec3 e(-5.0, -15.0, 0.0);
	glm::vec3 f(5.0, -15.0, 0.0);
	glm::vec3 G(10.0, -10.0, 0.0);
	glm::vec3 h(15.0, -5.0, 0.0);
	glm::vec3 i(15.0, 5.0, 0.0);
	glm::vec3 J(10.0, 10.0, 0.0);
	glm::vec3 k(5.0, 15.0, 0.0);
	glm::vec3 l(-5.0, 15.0, 0.0);*/

	glm::vec3 A(-10.0, 10.0, 0.0);
	glm::vec3 b(-8.0, 5.0, 0.0);
	glm::vec3 c(-15.0, -5.0, 0.0);
	glm::vec3 D(-10.0, -10.0, 0.0);
	glm::vec3 e(-5.0, -15.0, 0.0);
	glm::vec3 f(5.0, -15.0, 0.0);
	glm::vec3 G(10.0, -10.0, 0.0);
	glm::vec3 h(15.0, -5.0, 0.0);
	glm::vec3 i(15.0, 5.0, 0.0);
	glm::vec3 J(10.0, 10.0, 0.0);
	glm::vec3 k(5.0, 30.0, 0.0);
	glm::vec3 l(-5.0, 30.0, 0.0);

	AddBezierCurve(A, b, c, D);
	AddBezierCurve(D, e, f, G);
	AddBezierCurve(G, h, i, J);
	AddBezierCurve(J, k, l, A);
}

void destroy()
{
	glDetachShader(program, vshader);
	glDetachShader(program, fshader);
	glDeleteShader(vshader);
	glDeleteShader(fshader);
	glDeleteProgram(program);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	if (inPos != -1) glDeleteBuffers(1, &posVBO);
	if (inColor != -1) glDeleteBuffers(1, &colorVBO);
	if (inNormal != -1) glDeleteBuffers(1, &normalVBO);
	if (inTexCoord != -1) glDeleteBuffers(1, &texCoordVBO);
	glDeleteBuffers(1, &triangleIndexVBO);

	glBindVertexArray(0);
	glDeleteVertexArrays(1, &vao);

	glBindTexture(GL_TEXTURE_2D, 0);
	glDeleteTextures(1, &colorTexId);
}

void initShaderFw(const char *vname, const char *fname)
{
	vshader = loadShader(vname, GL_VERTEX_SHADER);
	fshader = loadShader(fname, GL_FRAGMENT_SHADER);

	program = glCreateProgram();
	glAttachShader(program, vshader);
	glAttachShader(program, fshader);

	glBindAttribLocation(program, 0, "inPos");
	glBindAttribLocation(program, 1, "inColor");
	glBindAttribLocation(program, 2, "inNormal");
	glBindAttribLocation(program, 3, "inTexCoord");

	glLinkProgram(program);

	int linked;
	glGetProgramiv(program, GL_LINK_STATUS, &linked);
	if (!linked)
	{
		//Calculamos una cadena de error
		GLint logLen;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLen);

		char *logString = new char[logLen];
		glGetProgramInfoLog(program, logLen, NULL, logString);
		std::cout << "Error: " << logString << std::endl;
		delete logString;

		glDeleteProgram(program);
		program = 0;
		exit(-1);
	}

	uNormalMat = glGetUniformLocation(program, "normal");
	uModelViewMat = glGetUniformLocation(program, "modelView");
	uModelViewProjMat = glGetUniformLocation(program, "modelViewProj");

	uColorTex = glGetUniformLocation(program, "colorTex");

	inPos = glGetAttribLocation(program, "inPos");
	inColor = glGetAttribLocation(program, "inColor");
	inNormal = glGetAttribLocation(program, "inNormal");
	inTexCoord = glGetAttribLocation(program, "inTexCoord");
}

void initObj()
{
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	if (inPos != -1)
	{
		glGenBuffers(1, &posVBO);
		glBindBuffer(GL_ARRAY_BUFFER, posVBO);
		glBufferData(GL_ARRAY_BUFFER, cubeNVertex*sizeof(float) * 3,
			cubeVertexPos, GL_STATIC_DRAW);
		glVertexAttribPointer(inPos, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(inPos);
	}

	if (inColor != -1)
	{
		glGenBuffers(1, &colorVBO);
		glBindBuffer(GL_ARRAY_BUFFER, colorVBO);
		glBufferData(GL_ARRAY_BUFFER, cubeNVertex*sizeof(float) * 3,
			cubeVertexColor, GL_STATIC_DRAW);
		glVertexAttribPointer(inColor, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(inColor);
	}

	if (inNormal != -1)
	{
		glGenBuffers(1, &normalVBO);
		glBindBuffer(GL_ARRAY_BUFFER, normalVBO);
		glBufferData(GL_ARRAY_BUFFER, cubeNVertex*sizeof(float) * 3,
			cubeVertexNormal, GL_STATIC_DRAW);
		glVertexAttribPointer(inNormal, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(inNormal);
	}


	if (inTexCoord != -1)
	{
		glGenBuffers(1, &texCoordVBO);
		glBindBuffer(GL_ARRAY_BUFFER, texCoordVBO);
		glBufferData(GL_ARRAY_BUFFER, cubeNVertex*sizeof(float) * 2,
			cubeVertexTexCoord, GL_STATIC_DRAW);
		glVertexAttribPointer(inTexCoord, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(inTexCoord);
	}

	glGenBuffers(1, &triangleIndexVBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, triangleIndexVBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,
		cubeNTriangleIndex*sizeof(unsigned int) * 3, cubeTriangleIndex,
		GL_STATIC_DRAW);
	
	colorTexId = PerlinN_LoadTex();
	//colorTexId = CellN_LoadTex();
}

GLuint loadShader(const char *fileName, GLenum type)
{
	unsigned int fileLen;
	char *source = loadStringFromFile(fileName, fileLen);

	//////////////////////////////////////////////
	//Creación y compilación del Shader
	GLuint shader;
	shader = glCreateShader(type);
	glShaderSource(shader, 1,
		(const GLchar **)&source, (const GLint *)&fileLen);
	glCompileShader(shader);
	delete source;

	//Comprobamos que se compilo bien
	GLint compiled;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
	if (!compiled)
	{
		//Calculamos una cadena de error
		GLint logLen;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLen);

		char *logString = new char[logLen];
		glGetShaderInfoLog(shader, logLen, NULL, logString);
		std::cout << "Error: " << logString << std::endl;
		delete logString;

		glDeleteShader(shader);
		exit(-1);
	}

	return shader;
}

void renderFunc()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	/**/
	glUseProgram(program);

	//Texturas
	if (uColorTex != -1)
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, colorTexId);
		glUniform1i(uColorTex, 0);
	}

	//renderCube();
	
	//renderPlane();

	// Draw mini cubes rotating
	//std::srand(RAND_SEED);

	glm::mat4 currentModel = model;
	float wallDisplacement = 3.0f;

	for (unsigned int i = 0; i < points; i+=5)
	{
		/*float size = float(std::rand() % 3 + 1);

		glm::vec3 axis(glm::vec3(float(std::rand() % 2),
			float(std::rand() % 2), float(std::rand() % 2)));
		if (glm::all(glm::equal(axis, glm::vec3(0.0f))))
			axis = glm::vec3(1.0f);

		float trans = float(std::rand() % 7 + 3) * 1.00f + 0.5f;
		glm::vec3 transVec = axis * trans;
		transVec.x *= (std::rand() % 2) ? 1.0f : -1.0f;
		transVec.y *= (std::rand() % 2) ? 1.0f : -1.0f;
		transVec.z *= (std::rand() % 2) ? 1.0f : -1.0f;

		model = glm::rotate(glm::mat4(1.0f), angle*2.0f*size, axis);
		model = glm::translate(model, transVec);
		model = glm::rotate(model, angle*2.0f*size, axis);
		model = glm::scale(model, glm::vec3(1.0f / (size*0.7f)));*/

		model[3].x = BezierPoints[i].x;
		model[3].y = 0.0f;
		model[3].z = BezierPoints[i].y;
		
		float omega = acos(glm::dot(glm::vec3(1.0f, 0.0f, 0.0f), glm::vec3(BezierTangents[i].x, 0.0f, BezierTangents[i].y)));

		model = glm::rotate(model, omega, glm::vec3(0.0f, 1.0f, 0.0f));
		model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));

		renderPlane();

		model = currentModel;
	}

	// Pared izquierda
	for (unsigned int i = 0; i < points; i += 5)
	{
		float omega = acos(glm::dot(glm::vec3(1.0f, 0.0f, 0.0f), glm::vec3(BezierTangents[i].x, 0.0f, BezierTangents[i].y)));

		model[3].x = BezierPoints[i].x;
		model[3].y = 2.0f;
		model[3].z = BezierPoints[i].y;

		glm::vec3 axis = glm::cross(glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(BezierTangents[i].x, 0.0f, BezierTangents[i].y));
		model = glm::translate(model, axis * wallDisplacement);

		model = glm::rotate(model, omega, glm::vec3(0.0f, 1.0f, 0.0f));

		renderPlane();

		model = currentModel;
	}

	// Pared derecha
	for (unsigned int i = 0; i < points; i += 5)
	{

		float omega = acos(glm::dot(glm::vec3(1.0f, 0.0f, 0.0f), glm::vec3(BezierTangents[i].x, 0.0f, BezierTangents[i].y)));

		model[3].x = BezierPoints[i].x;
		model[3].y = 2.0f;
		model[3].z = BezierPoints[i].y;

		glm::vec3 axis = glm::cross(glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(BezierTangents[i].x, 0.0f, BezierTangents[i].y));
		model = glm::translate(model, axis * -wallDisplacement);

		model = glm::rotate(model, omega, glm::vec3(0.0f, 1.0f, 0.0f));

		renderPlane();

		model = currentModel;
	}

	//*/

	glutSwapBuffers();
}

void renderCube()
{
	glm::mat4 modelView = view * model;
	glm::mat4 modelViewProj = proj * view * model;
	glm::mat4 normal = glm::transpose(glm::inverse(modelView));

	if (uModelViewMat != -1)
		glUniformMatrix4fv(uModelViewMat, 1, GL_FALSE,
		&(modelView[0][0]));
	if (uModelViewProjMat != -1)
		glUniformMatrix4fv(uModelViewProjMat, 1, GL_FALSE,
		&(modelViewProj[0][0]));
	if (uNormalMat != -1)
		glUniformMatrix4fv(uNormalMat, 1, GL_FALSE,
		&(normal[0][0]));
	
	glBindVertexArray(vao);
	glDrawElements(GL_TRIANGLES, cubeNVertex * 3,
		GL_UNSIGNED_INT, (void*)0);
}

void renderPlane()
{
	glm::mat4 modelView = view * model;
	glm::mat4 modelViewProj = proj * view * model;
	glm::mat4 normal = glm::transpose(glm::inverse(modelView));

	if (uModelViewMat != -1)
		glUniformMatrix4fv(uModelViewMat, 1, GL_FALSE,
		&(modelView[0][0]));
	if (uModelViewProjMat != -1)
		glUniformMatrix4fv(uModelViewProjMat, 1, GL_FALSE,
		&(modelViewProj[0][0]));
	if (uNormalMat != -1)
		glUniformMatrix4fv(uNormalMat, 1, GL_FALSE,
		&(normal[0][0]));

	glBindVertexArray(vao);
	glDrawElements(GL_TRIANGLES, planeNVertex * 3,
		GL_UNSIGNED_INT, (void*)0);
}

void resizeFunc(int width, int height)
{
	glViewport(0, 0, width, height);
	proj = glm::perspective(glm::radians(60.0f), float(width) /float(height), 1.0f, 50.0f);

	glutPostRedisplay();
}

void idleFunc()
{
	angle = (angle > 3.141592f * 2.0f) ? 0 : angle + 0.02f;
	
	glutPostRedisplay();
}

void keyboardFunc(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 'w': 
		view = glm::lookAt(glm::vec3(BezierPoints[currentPoint].x, 2.0f, BezierPoints[currentPoint].y),
			glm::vec3(BezierTangents[currentPoint].x, 0.0f, BezierTangents[currentPoint].y) + glm::vec3(BezierPoints[currentPoint].x, 2.0f, BezierPoints[currentPoint].y),
							glm::vec3(0.0f, 1.0f, 0.0f));

		if (currentPoint < points)
			currentPoint+=5;
		else
			currentPoint = 0;
		break;
	case 's':
		view = glm::rotate(view, glm::radians(-1.0f), glm::vec3(1.0f, 0.0f, 0.0f));
		break;
	case 'z':
		view[3].z -= 1.0f;
		break;
	case '+':
		view[3].z += 1.0f;
		break;
	default:
			break;
	
	}
}
void mouseFunc(int button, int state, int x, int y){}

void AddBezierCurve(glm::vec3 a, glm::vec3 b, glm::vec3 c, glm::vec3 d)
{
	for (int i = 0; i < CURVE_POINTS; ++i)
	{
		float t = static_cast<float>(i) / (CURVE_POINTS - 1);
		BezierPoints[points + i] = CalculateBezierPoint(t, a, b, c, d);
		BezierTangents[points + i] = CalculateBezierPointTangent(t, a, b, c, d);
	}
	points += CURVE_POINTS;
}

glm::vec3 CalculateBezierPoint(float t,
	glm::vec3 p0, glm::vec3 p1, glm::vec3 p2, glm::vec3 p3)
{
	float u = 1.0f;
	u -= t;
	float tt = t*t;
	float uu = u*u;
	float uuu = uu * u;
	float ttt = tt * t;

	glm::vec3 p = uuu * p0; //first term
	p += 3.0f * uu * t * p1; //second term
	p += 3.0f * u * tt * p2; //third term
	p += ttt * p3; //fourth term

	return p;
}

glm::vec3 CalculateBezierPointTangent(float t, glm::vec3 p0, glm::vec3 p1, glm::vec3 p2, glm::vec3 p3)
{
	float u = 1.0f;
	u -= t;
	float tt = t*t;
	float uu = u*u;

	glm::vec3 p = 3.0f * uu * (p1 - p0); //first term
	p += 6.0f * u * t * (p2 - p1); //second term
	p += 3.0f * tt * (p3 - p2); //third term

	return glm::normalize(p);
}


// Perlin Noise Functions
unsigned int PerlinN_LoadTex()
{
	PerlinN_GenerateTex();

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	unsigned int texId;
	glGenTextures(1, &texId);
	glBindTexture(GL_TEXTURE_2D, texId);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
		GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
		GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, PerlinN_Width,
		PerlinN_Height, 0, GL_RGBA, GL_UNSIGNED_BYTE,
		texData);

	//delete[] texData;
	//glGenerateMipmap(GL_TEXTURE_2D);

	return texId;
}

int PerlinN_Rand()
{
	int randValue = rand() % 140 - 70;
	return randValue;
}

void PerlinN_InitTex()
{
	int i, j, c, r, g, b;

	// Sand Colors
	int rSand = 189;
	int gSand = 146;
	int bSand = 104;

	int perlinValue;
	float colorContrast = 70.0f;

	for (i = 0; i < PerlinN_Height; i = i+PerlinN_GridSize)
	{
		for (j = 0; j < PerlinN_Width; j = j+PerlinN_GridSize)
		{
			perlinValue = PerlinN_Rand();//perlin(i, j) * colorContrast; // PerlinN_Rand();

			r = (rSand + perlinValue);
			g = (gSand + perlinValue);
			b = (bSand + perlinValue);

			// Clamp RGB values 0-255
			if (r < 0) r = 0;
			if (r > 255) r = 255;
			if (g < 0) g = 0;
			if (g > 255) g = 255;
			if (b < 0) b = 0;
			if (b > 255) b = 255;
			//std::cout << "R: " << r  << " G: " << g << " B: " << b << std::endl;

			PerlinN_TexAux2[i][j][0] = (GLubyte)r;
			PerlinN_TexAux2[i][j][1] = (GLubyte)g;
			PerlinN_TexAux2[i][j][2] = (GLubyte)b;

			if (i == 0)
			{
				PerlinN_TexAux2[PerlinN_Height][j][0] = (GLubyte)r;
				PerlinN_TexAux2[PerlinN_Height][j][1] = (GLubyte)g;
				PerlinN_TexAux2[PerlinN_Height][j][2] = (GLubyte)b;
			}

			if (j == 0)
			{
				PerlinN_TexAux2[i][PerlinN_Width][0] = (GLubyte)r;
				PerlinN_TexAux2[i][PerlinN_Width][1] = (GLubyte)g;
				PerlinN_TexAux2[i][PerlinN_Width][2] = (GLubyte)b;
			}
		}
	}

	PerlinN_TexAux2[PerlinN_Height][PerlinN_Width][0] = PerlinN_TexAux2[0][0][0];
	PerlinN_TexAux2[PerlinN_Height][PerlinN_Width][1] = PerlinN_TexAux2[0][0][1];
	PerlinN_TexAux2[PerlinN_Height][PerlinN_Width][2] = PerlinN_TexAux2[0][0][2];
}

void PerlinN_InterpolatePoint(int x, int y)
{
	int vx = (x / PerlinN_GridSize) * PerlinN_GridSize;
	int vy = (y / PerlinN_GridSize) * PerlinN_GridSize;

	float pctX = (x - vx) / (float)PerlinN_GridSize;
	float pctY = (y - vy) / (float)PerlinN_GridSize;

	PerlinN_TexAux2[x][y][0] = (PerlinN_TexAux2[vx][vy][0] * (1 - pctX) + PerlinN_TexAux2[vx + PerlinN_GridSize][vy][0] * pctX) * (1 - pctY) + (PerlinN_TexAux2[vx][vy + PerlinN_GridSize][0] * (1 - pctX) + PerlinN_TexAux2[vx + PerlinN_GridSize][vy + PerlinN_GridSize][0] * pctX) * pctY;
	PerlinN_TexAux2[x][y][1] = (PerlinN_TexAux2[vx][vy][1] * (1 - pctX) + PerlinN_TexAux2[vx + PerlinN_GridSize][vy][1] * pctX) * (1 - pctY) + (PerlinN_TexAux2[vx][vy + PerlinN_GridSize][1] * (1 - pctX) + PerlinN_TexAux2[vx + PerlinN_GridSize][vy + PerlinN_GridSize][1] * pctX) * pctY;
	PerlinN_TexAux2[x][y][2] = (PerlinN_TexAux2[vx][vy][2] * (1 - pctX) + PerlinN_TexAux2[vx + PerlinN_GridSize][vy][2] * pctX) * (1 - pctY) + (PerlinN_TexAux2[vx][vy + PerlinN_GridSize][2] * (1 - pctX) + PerlinN_TexAux2[vx + PerlinN_GridSize][vy + PerlinN_GridSize][2] * pctX) * pctY;
}

void PerlinN_InterpolateTex()
{
	int i, j;

	for (i = 0; i < PerlinN_Height; i++)
	{
		for (j = 0; j < PerlinN_Width; j++)
		{
			PerlinN_InterpolatePoint(i,j);
		}
	}

	for (i = 0; i < PerlinN_Height; i++)
	{
		for (j = 0; j < PerlinN_Width; j++)
		{
			PerlinN_TexAux1[i][j][0] += PerlinN_TexAux2[i][j][0];
			PerlinN_TexAux1[i][j][1] += PerlinN_TexAux2[i][j][1];
			PerlinN_TexAux1[i][j][2] += PerlinN_TexAux2[i][j][2];
		}
	}
}

void PerlinN_GenerateTex()
{
	for (int i = 0; i < PerlinN_Height; i++)
	{
		for (int j = 0; j < PerlinN_Width; j++)
		{
			PerlinN_TexAux1[i][j][0] = 0;
			PerlinN_TexAux1[i][j][1] = 0;
			PerlinN_TexAux1[i][j][2] = 0;
			PerlinN_TexAux1[i][j][3] = 255;
		}
	}

	for (int i = 0; i < PerlinN_FbmLayers; i++)
	{
		PerlinN_GridSize = pow(2, i);

		PerlinN_InitTex();
		PerlinN_InterpolateTex();
	}

	for (int i = 0; i < PerlinN_Height; i++)
	{
		for (int j = 0; j < PerlinN_Width; j++)
		{
			PerlinN_TexAux1[i][j][0] /= PerlinN_FbmLayers;
			PerlinN_TexAux1[i][j][1] /= PerlinN_FbmLayers;
			PerlinN_TexAux1[i][j][2] /= PerlinN_FbmLayers;
		}
	}

	for (int i = 0; i < PerlinN_Height; i++)
	{
		for (int j = 0; j < PerlinN_Width; j++)
		{
			texData[i][j][0] = (GLubyte)PerlinN_TexAux1[i][j][0];
			texData[i][j][1] = (GLubyte)PerlinN_TexAux1[i][j][1];
			texData[i][j][2] = (GLubyte)PerlinN_TexAux1[i][j][2];
			texData[i][j][3] = (GLubyte)PerlinN_TexAux1[i][j][3];
		}
	}
}


// Cell Noise (Worley)

unsigned int CellN_LoadTex()
{
	CellN_GenerateTex();

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	unsigned int texId;
	glGenTextures(1, &texId);
	glBindTexture(GL_TEXTURE_2D, texId);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
		GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
		GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, CellN_Width,
		CellN_Height, 0, GL_RGBA, GL_UNSIGNED_BYTE,
		texData);

	//delete[] texData;
	//glGenerateMipmap(GL_TEXTURE_2D);

	return texId;
}

int CellN_RandNumFP()
{
	int randValue = rand() % 3 + 1;
	return randValue;
}

int CellN_RandCoordFP()
{
	int randValue = rand() % CellN_GridSize;
	return randValue;
}

void CellN_GenerateTex()
{
	CellN_InitTex();

	for (int i = 0; i < CellN_Height; i++)
	{
		for (int j = 0; j < CellN_Width; j++)
		{
			texData[i][j][0] = (GLubyte)CellN_TexAux[i + CellN_GridSize][j + CellN_GridSize][0];
			texData[i][j][1] = (GLubyte)CellN_TexAux[i + CellN_GridSize][j + CellN_GridSize][1];
			texData[i][j][2] = (GLubyte)CellN_TexAux[i + CellN_GridSize][j + CellN_GridSize][2];
			texData[i][j][3] = (GLubyte)255;
		}
	}
}

void CellN_InitTex()
{
	int CellN_W = CellN_Width + CellN_GridSize;
	int CellN_H = CellN_Height + CellN_GridSize;
	int nFeaturedPoints;

	int coordX, coordY;

	for (int i = 0; i < CellN_H; i = i + CellN_GridSize)
	{
		for (int j = 0; j < CellN_W; j = j + CellN_GridSize)
		{
			nFeaturedPoints = CellN_RandNumFP();

			for (int k = 0; k < nFeaturedPoints; k++)
			{
				coordX = CellN_RandCoordFP();
				coordY = CellN_RandCoordFP();

				CellN_TexAux[i + coordX][j + coordY][0] = 255;
				CellN_TexAux[i + coordX][j + coordY][1] = 255;
				CellN_TexAux[i + coordX][j + coordY][2] = 255;

				if (i == 0)
				{
					CellN_TexAux[CellN_H + coordX][j + coordY][0] = 255;
					CellN_TexAux[CellN_H + coordX][j + coordY][1] = 255;
					CellN_TexAux[CellN_H + coordX][j + coordY][2] = 255;
				}
				if (j == 0)
				{
					CellN_TexAux[i + coordX][CellN_W + coordY][0] = 255;
					CellN_TexAux[i + coordX][CellN_W + coordY][1] = 255;
					CellN_TexAux[i + coordX][CellN_W + coordY][2] = 255;
				}
				if (i == 0 && j == 0)
				{
					CellN_TexAux[CellN_H + coordX][CellN_W + coordY][0] = 255;
					CellN_TexAux[CellN_H + coordX][CellN_W + coordY][1] = 255;
					CellN_TexAux[CellN_H + coordX][CellN_W + coordY][2] = 255;
				}
			}
		}
	}

	int max_dist;
	for (int i = CellN_GridSize; i < CellN_Height; i++)
	{
		for (int j = CellN_GridSize; j < CellN_Width; j++)
		{
			max_dist = 0;

		}
	}
}

int euclideanDistance(int x1, int y1, int x2, int y2)
{
	int X = x1 - x2;
	int Y = y1 - y2;
	return X*X + Y*Y;
}