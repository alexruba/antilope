#version 330 core

out vec4 outColor;

in vec3 color;
in vec3 pos;
in vec3 norm;
in vec2 texCoord;

uniform sampler2D colorTex;

//Propiedades del objeto
vec3 Ka;
vec3 Kd;
vec3 Ks;
vec3 N;
float alpha = 500.0;
vec3 Ke;

//Propiedades de la luz
vec3 Ia = vec3 (0.3);
vec3 Id = vec3 (1.0);
vec3 Is = vec3 (0.7);
vec3 lpos = vec3 (0.0); 

vec3 shade();
const vec2 size = vec2(0.3,0.0);
const ivec3 off = ivec3(-1,0,1);

void main()
{
	Ka = texture(colorTex, texCoord).rgb;
	Kd = texture(colorTex, texCoord).rgb;
	Ks = vec3 (0.0);

	//N = normalize (norm);
	
	//outColor = vec4(shade(), 1.0);
	//outColor = vec4(texture(colorTex, texCoord).rgb, 1.0);

	// Height Map -> Normal Map
	vec4 tex = texture(colorTex, texCoord);
    float s11 = tex.x;
    float s01 = textureOffset(colorTex, texCoord, off.xy).x;
    float s21 = textureOffset(colorTex, texCoord, off.zy).x;
    float s10 = textureOffset(colorTex, texCoord, off.yx).x;
    float s12 = textureOffset(colorTex, texCoord, off.yz).x;
    vec3 va = normalize(vec3(size.xy,s21-s01));
    vec3 vb = normalize(vec3(size.yx,s12-s10));
    N = vec3(vec4( cross(va,vb), s11 )).xyz;
  
	// Determine where the light is positioned (this can be set however you like)  
	//vec3 light_pos = normalize(vec3(1.0, 1.0, 1.5));  
  
	// Calculate the lighting diffuse value  
	//float diffuse = max(dot(bump.xyz, light_pos), 0.0);  
  
	//vec3 color = diffuse * texture2D(colorTex, texCoord.st).rgb;  
  
	// Set the output color of our current pixel  
	outColor = vec4(shade(), 1.0);  
}

vec3 shade()
{
	vec3 c = vec3(0.0);
	c = Ia * Ka;

	vec3 L = normalize (lpos - pos);
	vec3 diffuse = Id * Kd * dot (L,N);
	c += clamp(diffuse, 0.0, 1.0);
	
	vec3 V = normalize (-pos);
	vec3 R = normalize (reflect (-L,N));
	float factor = max (dot (R,V), 0.01);
	vec3 specular = Is*Ks*pow(factor,alpha);
	c += clamp(specular, 0.0, 1.0);

	return c;
}
